import getpass
import telnetlib
import time
import wsgiserver
import urllib.parse as urlparse


hosts = ['', '']
user = ""
password = ""
port = 0


# state = 'up'


def down_up(state, host, port):
    tn = telnetlib.Telnet(host)

    tn.read_until(b':')
    tn.write((user + '\n').encode('ascii'))

    tn.read_until(b':')
    tn.write((password + '\n').encode('ascii'))
    time.sleep(1)

    tn.read_until(b'#', timeout=20)
    tn.write(('conf t' + '\n').encode('ascii'))
    tn.read_until(b'config', timeout=20)

    s = 'int eth 1/0/%s' % port
    tn.write((s + '\n').encode('ascii'))
    tn.read_until(b'#', timeout=20)

    if state == 'down':
        tn.write(('shut' + '\n').encode('ascii'))
    else:
        tn.write(('no shut' + '\n').encode('ascii'))
    tn.close()


def http_sw(environ, start_response):
    start_response('200 OK', [
        ('Content-Type', 'text/html; charset=utf-8')
    ])

    qs = urlparse.parse_qs(environ['QUERY_STRING'])
    env_p = environ['PATH_INFO'][1:]

    if env_p != "" and env_p == "sw":
        state = qs.get('state', [''])[0]
        if state:
            for host in hosts:
                port = 0
                if host == '':
                    port = 44
                elif host == '':
                    port = 46
                elif host == '':
                    port = 49
                elif host == '':
                    port = 44
                elif host == '':
                    port = 44
                if port != 0:
                    down_up(state, host, port)

    return [b'OK']


server = wsgiserver.WSGIServer(http_sw, host='', port='')
server.start()
