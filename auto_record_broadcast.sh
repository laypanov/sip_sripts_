#!/bin/sh
pause=0
spooldir=/var/spool/asterisk # No trailing slash!
diallist=/var/spool/asterisk/scripts/broadcast_record_list.txt

#Завершаем все звонки
asterisk -rx "channel request hangup all"

echo `date`": Dialing with $pause second pause"

while read number; do

echo "Channel: PJSIP/$number
MaxRetries: 0
RetryTime: 1
WaitTime: 30
Context: broadcast_call
Extension: 4410
Callerid: ИНФОРМИРОВАНИЕ
Account: autodialer
Priority: 1"  > $spooldir/tmp/$number

chmod 777 $spooldir/tmp/$number
chown asterisk:asterisk $spooldir/tmp/$number
mv $spooldir/tmp/$number $spooldir/outgoing/

echo "$number"
sleep $pause

done < $diallist
echo "Done"
