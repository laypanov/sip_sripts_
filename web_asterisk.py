import wsgiserver
import subprocess, signal, os
import json


def my_app(environ, start_response):
    status = '200 OK'
    response_headers = [('Content-type', 'application/json')]
    start_response(status, response_headers)
    if environ['PATH_INFO'][1:-1] == "favicon.ic":
        return [b'favicon']
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    request_body = environ['wsgi.input'].read(request_body_size)
    if request_body:
        data = json.loads(request_body)

        number = data['number']
        cidname = data['cidname']
        dial = data['dial']
        delay = data['delay']
        if delay == "insert":
            subprocess.run(["asterisk", "-x",
                            "database query \"INSERT into astdb (key,value) VALUES ('/cidname/{0}','{1}')\"".format(
                                number, cidname)])
            subprocess.run(["asterisk", "-x",
                            "database query \"INSERT into astdb (key,value) VALUES ('/sysspeeddials/{0}','{1}')\"".format(
                                dial, number)])
        elif delay == "delete":
            subprocess.run(
                ["asterisk", "-x", "database query \"DELETE FROM  astdb WHERE key='/cidname/{0}'\"".format(number)])
            subprocess.run(
                ["asterisk", "-x", "database query \"DELETE FROM  astdb WHERE key='/sysspeeddials/{0}'\"".format(dial)])
    # get variables
    out = []
    dial_id = []
    dial_number = []
    sysspeeddials = os.popen("asterisk -x 'database show sysspeeddials'").read()
    sysspeeddials = sysspeeddials.splitlines()

    for dial in sysspeeddials:
        try:
            dial = dial.split(': ')
            dial_id.append(dial[0].replace("/sysspeeddials/", "").strip())
            dial_number.append(dial[1].strip())
        except:
            pass

    cidnames = os.popen("asterisk -x 'database show cidname'").read()
    cidnames = cidnames.splitlines()
    for cid in cidnames:
        try:
            cid = cid.split(': ')
            cn = cid[0].replace("/cidname/", "").strip()
            cid_name = cid[1].strip()
            for pos, dn in enumerate(dial_number):
                if dn == cn:
                    out.append("{0}, {1}, {2}".format(dn, dial_id[pos], cid_name))
        except:
            pass

    out = json.dumps(out).encode('utf-8')
    return [out]


server = wsgiserver.WSGIServer(my_app, host='', port='')
server.start()
